
import java.util.*;
// Resource: https://www.techiedelight.com/huffman-coding/

/** Prefix codes and Huffman tree.
 * Tree depends on source data.
 */
public class Huffman {

   private final byte[] original;
   private final Node root;
   private Map<Byte,String> prefixCode;
   private int lenOfBits;

   /** Constructor to build the Huffman code for a given bytearray.
    * @param original source data
    */
   Huffman (byte[] original) {
      this.original = original;
      this.root = createHuffmanTree();
   }

   /** Node class for Huffman tree */
   static class Node {
      Byte b;
      int freq;
      Node left = null;
      Node right = null;

      Node(Byte b, int freq) {
         this.b = b;
         this.freq = freq;
      }

      public Node(Byte b, int freq, Node left, Node right) {
         this.b = b;
         this.freq = freq;
         this.left = left;
         this.right = right;
      }

      @Override
      public String toString() {
         return  b + "{freq=" + freq + ", left=" + left + ", right=" + right + "}";
      }
   }

   /** Length of encoded data in bits.
    * @return number of bits
    */
   public int bitLength() {
      return lenOfBits;
   }


   /** Encoding the byte array using this prefix code.
    * @param origData original data
    * @return encoded data
    */
   public byte[] encode (byte [] origData) {
      if (root == null)
         return null;

      prefixCode = new HashMap<Byte, String>();
      getPrefixCode(root, "", prefixCode);

      byte[] encoded = new byte[origData.length];
      
      // Encode bytes with prefix code
      for (int i = 0; i < encoded.length; i++) {
         Byte b = origData[i];
         String binaryCode = prefixCode.get(b);
         lenOfBits += binaryCode.length();

         encoded[i] = Byte.parseByte(binaryCode, 2);
      }
      return encoded;
   }

   /** Generating prefix code from Huffman tree.
    * @param root root Node
    * @param str node's binary value (string)
    * @param prefixCode prefix code map
    */
   public static void getPrefixCode(Node root, String str, Map<Byte,String> prefixCode) {
      if (root == null)
         return;

      // When is leaf node
      if (root.left == null && root.right == null) {
         // When byte array is of length 1 change str to value 0
         if (str.isEmpty()) {
            str = "0";
         }

         prefixCode.put(root.b, str);
      }

      getPrefixCode(root.left, str + "0", prefixCode);
      getPrefixCode(root.right, str + "1", prefixCode);
   }

   /** Decoding the byte array using this prefix code.
    * @param encodedData encoded data
    * @return decoded data (hopefully identical to original)
    */
   public byte[] decode(byte[] encodedData) {
      byte[] decoded = new byte[encodedData.length];

      for (int i = 0; i < encodedData.length; i++) {
         Byte b = encodedData[i];
         // Converting byte to binary value (byte to int to binary)
         String bToBinary = Integer.toBinaryString(b.intValue());

         decoded[i] = getOriginalByte(bToBinary);
      }

      return decoded;
   }

   /** Getting the original byte value with binary value from prefixCode.
    * @param binaryValue binary value
    * @return original byte
    */
   private Byte getOriginalByte(String binaryValue) {
      for (var entry : prefixCode.entrySet()) {
         if (entry.getValue().equals(binaryValue)) {
            return entry.getKey();
         }
      }
      throw new RuntimeException("Binary value " + binaryValue + " not found from prefix code!");
   }

   /** Creating Huffman tree.
    * @return root node
    */
   private Node createHuffmanTree() {
      // Get frequencies of each byte
      Map<Byte, Integer> freq = new HashMap<Byte, Integer>();
      for (byte b: original) {
         freq.put(b, freq.getOrDefault(b, 0) + 1);
      }

      // Create a priority queue to hold nodes
      PriorityQueue<Node> pq;
      pq = new PriorityQueue<Node>(Comparator.comparingInt(n -> n.freq));

      // Create a leaf node for each byte and add it to the priority queue.
      for (var entry : freq.entrySet()) {
         pq.add(new Node(entry.getKey(), entry.getValue()));
      }

      while (pq.size() > 1) {
         // Remove the two bytes of lowest frequency from the queue
         Node first = pq.poll();
         Node second = pq.poll();

         // Create a new node with first and second as children and add it to queue
         int sum = first.freq + second.freq;
         pq.add(new Node((byte) 0, sum, first, second));
      }
      return pq.poll();
   }

   /** Main method. */
   public static void main (String[] params) {
      String tekst = "AAAAAAAAAAAAABBBBBBCCCDDEEF";
      byte[] orig = tekst.getBytes();
      Huffman huf = new Huffman (orig);
      byte[] kood = huf.encode (orig);
      byte[] orig2 = huf.decode (kood);
      System.out.println(Arrays.toString(orig2));
      // must be equal: orig, orig2
      System.out.println (Arrays.equals (orig, orig2));
      int lngth = huf.bitLength();
      System.out.println ("Length of encoded data in bits: " + lngth);
   }
}
